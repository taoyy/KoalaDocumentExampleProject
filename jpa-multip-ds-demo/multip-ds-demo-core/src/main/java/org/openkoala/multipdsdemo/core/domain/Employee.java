package org.openkoala.multipdsdemo.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.dayatang.domain.AbstractEntity;
import org.openkoala.koala.commons.domain.KoalaAbstractEntity;

@Entity
@Table(name = "employees")
public class Employee extends KoalaAbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2700241427681303048L;

	private String name;
	
	private int age;
	
	private String gender;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;
	}

	public String toString() {
		return name;
	}

	@Override
	public String[] businessKeys() {
		return null;
	}
	
	
}
