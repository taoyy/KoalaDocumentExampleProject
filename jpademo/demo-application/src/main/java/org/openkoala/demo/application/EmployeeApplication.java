package org.openkoala.demo.application;

import java.util.List;

import org.openkoala.demo.core.domain.Employee;
import org.openkoala.demo.core.domain.Organization;

public interface EmployeeApplication {

	public Employee getEmployee(Long id);

	public Employee saveEmployee(Employee employee);

	public void updateEmployee(Employee employee);

	public void removeEmployee(Long id);

	public void removeEmployees(Long[] ids);

	public List<Employee> findAllEmployee();

	void assignEmployeeToOrganization(Employee employee, Organization organization);

	List<Employee> findEmployeesByAgeRange(Integer from, Integer to);

}
