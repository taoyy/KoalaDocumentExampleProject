<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form  class="form-horizontal">
	           <div class="form-group">
                    <label class="col-lg-3 control-label">name:</label>
                    <div class="col-lg-9">
                           <p class="form-control-static" id="nameID"></p>
                    </div>
                </div>
	           <div class="form-group">
                    <label class="col-lg-3 control-label">age:</label>
                    <div class="col-lg-9">
                           <p class="form-control-static" id="ageID"></p>
                    </div>
                </div>
	           <div class="form-group">
                    <label class="col-lg-3 control-label">gender:</label>
                    <div class="col-lg-9">
                           <p class="form-control-static" id="genderID"></p>
                    </div>
                </div>
</form>