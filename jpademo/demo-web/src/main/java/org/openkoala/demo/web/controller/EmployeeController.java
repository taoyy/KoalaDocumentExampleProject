package org.openkoala.demo.web.controller;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.dayatang.querychannel.Page;
import org.openkoala.demo.application.EmployeeApplication;
import org.openkoala.demo.facade.dto.*;
import org.openkoala.demo.facade.EmployeeFacade;

@Controller
@RequestMapping("/Employee")
public class EmployeeController {
		
	@Inject
	private EmployeeFacade employeeFacade;
	
	@ResponseBody
	@RequestMapping("/add")
	public Map<String, Object> add(EmployeeDTO employeeDTO) {
		Map<String, Object> result = new HashMap<String, Object>();
		employeeFacade.saveEmployee(employeeDTO);
		result.put("result", "success");
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/update")
	public Map<String, Object> update(EmployeeDTO employeeDTO) {
		Map<String, Object> result = new HashMap<String, Object>();
		employeeFacade.updateEmployee(employeeDTO);
		result.put("result", "success");
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/pageJson")
	public Page pageJson(EmployeeDTO employeeDTO, @RequestParam int page, @RequestParam int pagesize) {
		Page<EmployeeDTO> all = employeeFacade.pageQueryEmployee(employeeDTO, page, pagesize);
		return all;
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public Map<String, Object> delete(@RequestParam String ids) {
		Map<String, Object> result = new HashMap<String, Object>();
		String[] value = ids.split(",");
        Long[] idArrs = new Long[value.length];
        for (int i = 0; i < value.length; i ++) {
        	
        	        					idArrs[i] = Long.parseLong(value[i]);
						        	
        }
        employeeFacade.removeEmployees(idArrs);
		result.put("result", "success");
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/get/{id}")
	public Map<String, Object> get(@PathVariable Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("data", employeeFacade.getEmployee(id));
		return result;
	}
	
}
