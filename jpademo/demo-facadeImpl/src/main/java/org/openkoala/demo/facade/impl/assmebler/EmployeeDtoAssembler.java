package org.openkoala.demo.facade.impl.assmebler;

import org.openkoala.demo.core.domain.Employee;
import org.openkoala.demo.facade.dto.EmployeeDTO;

public class EmployeeDtoAssembler {

	public static EmployeeDTO EntityToDto(Employee employee) {
		EmployeeDTO result = new EmployeeDTO();
		result.setId(employee.getId());
		result.setName(employee.getName());
		result.setAge(employee.getAge());
		result.setGender(employee.getGender());
		return result;
	}

	public static Employee DtoToEntity(EmployeeDTO employeeDTO) {
		Employee result = new Employee();
		result.setId(employeeDTO.getId());
		result.setName(employeeDTO.getName());
		result.setAge(employeeDTO.getAge());
		result.setGender(employeeDTO.getGender());
		return result;
	}

}
