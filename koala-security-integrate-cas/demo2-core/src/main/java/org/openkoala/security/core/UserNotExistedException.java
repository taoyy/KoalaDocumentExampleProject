package org.openkoala.security.core;

public class UserNotExistedException extends SecurityException {

	private static final long serialVersionUID = 112937767297980508L;

	public UserNotExistedException() {
		super();
	}

	public UserNotExistedException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserNotExistedException(String message) {
		super(message);
	}

	public UserNotExistedException(Throwable cause) {
		super(cause);
	}

}
