package org.openkoala.mybatisdemo.core.domain;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openkoala.koala.util.KoalaBaseSpringTestCase;

/**
	领域层的测试类，直接继承KoalaBaseSpringTestCase
	
 */ 
 public class EmployeeTest extends KoalaBaseSpringTestCase { 
 
 	@Test
	public void add() {
		Employee employee = new Employee();
		employee.setName("lingen");
		employee.setBirthDate(new Date());
		employee.setGender("man");
		employee.setAge(21);
		employee.save();
	}

	@Test
	public void update() {
		Employee employee = new Employee();
		employee.setId(1l);
		employee.setName("lingen");
		employee.setBirthDate(new Date());
		employee.setGender("man");
		employee.setAge(21);
		employee.save();
	}

	@Test
	public void remove() {
		Employee employee = new Employee();
		employee.setId(1l);
		employee.remove();
	}

	@Test
	public void get() {
		Employee employee = Employee.getRepository().get(Employee.class, 10l);
		assertNull(employee);
	}

	@Test
	public void getAll() {
		Employee employee = new Employee();
		employee.setName("lingen");
		employee.setBirthDate(new Date());
		employee.setGender("man");
		employee.setAge(21);
		employee.save();
		
		List<Employee> employees = Employee.getRepository().findAll(Employee.class);
		assertNotNull(employees);
	}
 
	@Test
	public void testQueryByName() {
		Employee employee = new Employee();
		employee.setName("lingen");
		employee.setBirthDate(new Date());
		employee.setGender("man");
		employee.setAge(21);
		employee.save();
		List<Employee> employees = Employee.findByName("lingen");
		System.out.println(employees);
		assertNotNull(employees);
	}

 }
