package org.openkoala.mybatisdemo.facade;

import java.util.List;

import org.dayatang.querychannel.Page;
import org.openkoala.mybatisdemo.core.domain.Employee;
import org.openkoala.mybatisdemo.facade.dto.EmployeeDTO;

public interface EmployeeFacade {

	EmployeeDTO getEmployee(Long id);
	
	EmployeeDTO saveEmployee(EmployeeDTO employee);
	
	void updateEmployee(EmployeeDTO employee);
	
	void removeEmployee(Long id);
	
	void removeEmployees(Long[] ids);
	
	List<EmployeeDTO> findAllEmployee();
	
	Page<Employee> findEmployeeByName(String name,int currentPage,int pageSize);

}

