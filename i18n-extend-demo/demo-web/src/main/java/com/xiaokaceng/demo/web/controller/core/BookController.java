
package com.xiaokaceng.demo.web.controller.core;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.dayatang.querychannel.Page;
import com.xiaokaceng.demo.application.core.BookApplication;
import com.xiaokaceng.demo.application.dto.*;

@Controller
@RequestMapping("/Book")
public class BookController {
		
	@Inject
	private BookApplication bookApplication;
	
	@ResponseBody
	@RequestMapping("/add")
	public Map<String, Object> add(BookDTO bookDTO) {
		Map<String, Object> result = new HashMap<String, Object>();
		bookApplication.saveBook(bookDTO);
		result.put("result", "success");
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/update")
	public Map<String, Object> update(BookDTO bookDTO) {
		Map<String, Object> result = new HashMap<String, Object>();
		bookApplication.updateBook(bookDTO);
		result.put("result", "success");
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/pageJson")
	public Page pageJson(BookDTO bookDTO, @RequestParam int page, @RequestParam int pagesize) {
		Page<BookDTO> all = bookApplication.pageQueryBook(bookDTO, page, pagesize);
		return all;
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public Map<String, Object> delete(@RequestParam String ids) {
		Map<String, Object> result = new HashMap<String, Object>();
		String[] value = ids.split(",");
        Long[] idArrs = new Long[value.length];
        for (int i = 0; i < value.length; i ++) {
        	
        	        					idArrs[i] = Long.parseLong(value[i]);
						        	
        }
        bookApplication.removeBooks(idArrs);
		result.put("result", "success");
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/get/{id}")
	public Map<String, Object> get(@PathVariable Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("data", bookApplication.getBook(id));
		return result;
	}
	
		
}
