package org.openkoala.mybatismultipdsdemo.core.domain.secondds;

import java.util.List;

public class Address extends SecondDsBaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4388980540533120574L;

	/**
	 * ID主键，自增长
	 */
	private Long id;
	
	private String addr;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	@Override
	public boolean existed() {
		return false;
	}

	@Override
	public boolean notExisted() {
		return false;
	}

	public static List<Address> findByAddr(String addr) {
		List<Address> results = getRepository().createNamedQuery(Address.class,"findByAddr").setParameters(addr).list();
		return results;
	}

	@Override
	public String[] businessKeys() {
		return new String[]{addr};
	}

}
