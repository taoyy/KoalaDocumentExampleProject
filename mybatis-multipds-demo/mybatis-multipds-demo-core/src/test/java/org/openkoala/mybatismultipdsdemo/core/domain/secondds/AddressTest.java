package org.openkoala.mybatismultipdsdemo.core.domain.secondds;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.dayatang.domain.InstanceFactory;
import org.junit.Before;
import org.junit.Test;
import org.openkoala.koala.util.KoalaBaseSpringTestCase;
import org.springframework.test.context.transaction.TransactionConfiguration;

@TransactionConfiguration(transactionManager = "secondTransactionManager",defaultRollback = true) 
public class AddressTest extends KoalaBaseSpringTestCase {

	@Before
	public void before() throws SQLException{
		Statement stmt  = null;
		try{
		String create = " CREATE TABLE `Address` ( `id` int(11) DEFAULT NULL, `addr` varchar(255) DEFAULT NULL )";
		DataSource dataSource = InstanceFactory.getInstance(DataSource.class, "secondDataSource");
		stmt = dataSource.getConnection().createStatement();
		stmt.execute(create);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			stmt.close();
		}
	}

	@Test
	public void add() {
		Address address = new Address();
		address.setAddr("addresss");
		address.save();
	}

	@Test
	public void update() {
		Address address = new Address();
		address.setId(1l);
		address.setAddr("addresss2222");
		address.save();
	}

	@Test
	public void remove() {
		Address address = new Address();
		address.setId(1l);
		address.remove();
	}

	@Test
	public void testQueryByAddr() {
		Address address = new Address();
		address.setAddr("addresss");
		address.save();

		List<Address> addresses = Address.findByAddr("addresss");
		System.out.println(addresses);
		assertNotNull(addresses);
	}
	
}
